/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async function(knex) {
  await knex.schema.createTable('priorities', (table) => {
    table.increments('id')
    table.string('name').notNullable()
    table.integer('ordering').notNullable()
  })

  await knex.schema.alterTable('todos', (table) => {
    table.integer('priority_id').nullable()
    table.foreign('priority_id').references('priorities.id')
  })
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async function(knex) {
  await knex.schema.alterTable('todos', (table) => {
    table.dropColumn('priority_id')
  })
  await knex.schema.dropTable('priorities')
 
  
};
