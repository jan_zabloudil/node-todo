import  express  from "express";
import knex from "knex";
import knexfile from "./knexfile.js";

const app = express()
const db = knex(knexfile)

app.set('view engine', 'ejs')

app.use(express.urlencoded())

let todos = []

app.get('/', async (req, res) => {

  const priorities =  await db('priorities').select('*').orderBy('ordering', 'asc')
  const queryState = ! (req.query.state in [0,1]) ? 'all' : Number(req.query.state)
  const queryFulltext = req.query.fulltext


  todos =  await db('todos').join('priorities', 'todos.priority_id', '=', 'priorities.id')
                      .select('todos.id', 'todos.title', 'todos.done', 'priorities.id as priorityId', 'priorities.name AS priorityName')
                      .where((q) => {
                        if(queryState !== 'all'){
                          q.where('todos.done', queryState)
                        }  

                        if(queryFulltext){
                          q.andWhere('title', 'like', `%${queryFulltext}%`)
                        }
                      })

  res.render('index', {
    name: 'Honza',
    todos: todos,
    queryState: queryState,
    queryFulltext: queryFulltext,
    priorities: priorities
  })
})

app.post('/new-todo', async (req, res) => {

  const newTodo = {
    title: req.body.title,
    priority_id: req.body.priority
  }

  await db('todos').insert(newTodo)

  res.redirect('/')
})

app.get('/remove-todo/:id', async (req, res) => {
  const idToRemove = Number(req.params.id)

  await db('todos').delete().where('id', idToRemove)
  
  todos = todos.filter((todo) => todo.id !== idToRemove)

  res.redirect('/')
})

app.get('/change-status/:id', async (req, res) => {
  const idToToggle = Number(req.params.id)

  const todo = await db('todos').select('*').where('id', idToToggle).first()
  
  await db('todos').update({ done: !todo.done}).where('id', idToToggle)

  res.redirect('/')
})

app.get('/detail-todo/:id', async (req, res) => {
  const idToShow = Number(req.params.id)

  const todoToShow = await db('todos').select('*').where('id', idToShow).first()
  const priorities = await db('priorities').select('*')

  res.render('detail', {
    todo: todoToShow,
    priorities: priorities
  })
})

app.post('/update-todo/:id', async (req, res) => {
  const idToUpdate = Number(req.params.id)
  const newTitle = String(req.body.title)
  const priorityId = Number(req.body.priority)

  await db('todos').update({ title: newTitle, priority_id: priorityId }).where('id', idToUpdate)

  res.redirect('back')
})

app.listen(3000, () => {
  console.log('App listening')
});