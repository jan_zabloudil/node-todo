import test from 'ava'
import supertest from 'supertest'
import { app } from '../src/app.js'
import { db } from '../src/database.js'

test.beforeEach(async () => {
  await db.migrate.latest()
})

test.afterEach(async () => {
  await db.migrate.rollback()
})

test.serial('GET / shows title of the application', async (t) => {
  const response = await supertest(app).get('/')

  t.assert(response.text.includes('<h1>ToDos!</h1>'))
})

test.serial('GET / shows list of todos', async (t) => {
  await db('todos').insert({ title: 'Test todo!!!' })

  const response = await supertest(app).get('/')

  t.assert(response.text.includes('Test todo!!!'))
})

test.serial('POST /new-todo creates new todo', async (t) => {
  const response = await supertest(app)
    .post('/new-todo')
    .type('form')
    .send({ title: 'Test todo from form' })
    .redirects(1)

  t.assert(response.text.includes('Test todo from form'))
})

test.serial('GET /detail/todo/:id, GET /toggle-todo/:id, POST /update-todo/:id', async (t) => {
  await db('todos').insert({ title: 'Toto to test'})

  /* Test if todo detail is loaded correctly */
  let response = await supertest(app).get('/detail-todo/1')
  t.assert(response.text.includes('Toto to test'))
  t.assert(response.text.includes('Nesplněno'))

  /* Test toggle from false to true value */
  response = await supertest(app).get('/toggle-todo/1').redirects(1)
  t.assert(response.text.includes('Hotovo'))

  /* Test toggle from true to false value */
  response = await supertest(app).get('/toggle-todo/1').redirects(1)
  t.assert(response.text.includes('Nesplněno'))

  /* Test todo update */
  response = await supertest(app)
    .post('/update-todo/1')
    .type('form')
    .send({ title: 'Todo renamed' })
    .redirects(1)

  t.assert(response.text.includes('Todo renamed'))
})

test.serial('Test GET /toggle-todo/:id from GET /', async (t) => {
  await db('todos').insert({ title: 'Toto to test'})

  /* Test if todo detail is loaded correctly */
  let response = await supertest(app).get('/')
  t.assert(response.text.includes('Toto to test'))
  t.assert(response.text.includes('Nesplněno'))

  /* Test toggle from false to true value */
  response = await supertest(app).get('/toggle-todo/1').redirects(1)
  t.assert(response.text.includes('Toto to test'))
  t.assert(response.text.includes('Hotovo'))  

  /* Test toggle from true to false value */
  response = await supertest(app).get('/toggle-todo/1').redirects(1)
  t.assert(response.text.includes('Toto to test'))
  t.assert(response.text.includes('Nesplněno'))   
})

test.serial('POST /new-todo validation', async (t) => {
  const response = await supertest(app)
    .post('/new-todo')
    .type('form')
    .send({ title: '' })
    .redirects(1)

  t.assert(response.text.includes('Chyba formuláře: název todo musí být vyplněn'))
})

test.serial('POST /update-todo/:id validation', async (t) => {
  await db('todos').insert({ title: 'Toto to test'})

  const response = await supertest(app)
    .post('/update-todo/1')
    .type('form')
    .send({ title: '' })
    .redirects(1)

  t.assert(response.text.includes('Chyba formuláře: název todo musí být vyplněn'))
})